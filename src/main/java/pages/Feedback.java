package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

public class Feedback {
	
	WebDriver driver;
	String verificationKeyWords = "WE'VE RECEIVED YOUR REQUEST";
	
	public Feedback(WebDriver driver){
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how=How.XPATH , using="//h1[contains(text(),'RECEIVED YOUR REQUEST')]") WebElement feedbackKeyWords;
	
	public void verifyFeedbackKeyWords(){
		Assert.assertEquals(feedbackKeyWords.getText(), verificationKeyWords, "Expected key words not presented");
	}
	
	
}
