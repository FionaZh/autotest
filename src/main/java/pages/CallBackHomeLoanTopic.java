package pages;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import io.github.sukgu.*;

public class CallBackHomeLoanTopic {
	
	WebDriver driver;
	Shadow shadow;
	String verificationKeyWords = "Which home loan topic can we help you with?";
	
	public CallBackHomeLoanTopic(WebDriver driver){
		this.driver=driver;
		//PageFactory.initElements(driver, this);
		shadow = new Shadow(driver);
	}
	
	
	
	//@FindBy(how=How.TAG_NAME , using="myRadioButton-label") WebElement callBackHomeLoanTopicKeyWords;
	
	public void clickOnNewHomeLoansRadio(){
		getNewHomeLoansRadio().click();
	}

	public void clickOnNextButton(){
		shadow.scrollTo(getNextButton());
		getNextButton().click();

	}

	public void verifyHomeLoanTopicKeyWords(){
		//Assert.assertEquals(callBackHomeLoanTopicKeyWords.getText(), verificationKeyWords, "Expected key words not presented");
	}
	
	//New home loans radio in #shasow-root
	public WebElement getNewHomeLoansRadio()
	{
		WebElement newHomeLoansRadio=null;
		
		try {
			newHomeLoansRadio = shadow.findElement("label[data-component-id='Radio']");
		
		}catch(Exception e)
		{
			System.out.println("Getting New Home Loans Radio failed.");
		}
		
		return newHomeLoansRadio;
	}
	
	//Next button in #shasow-root
	public WebElement getNextButton()
	{
		WebElement nextButton=null;
		
		try {
			nextButton = shadow.findElement("button[data-component-id='Button']");
		
		}catch(Exception e)
		{
			System.out.println("Getting New Home Loans Radio failed.");
		}
		
		return nextButton;
	}
    

}
