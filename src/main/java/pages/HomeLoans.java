package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class HomeLoans {
	WebDriver driver;
	WebDriverWait wait20;
	String verificationKeyWords = "Home loans";
	
	public HomeLoans(WebDriver driver){
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how=How.XPATH , using="//span[contains(text(),'Request a call back')]") WebElement callBackLink;
	@FindBy(how=How.TAG_NAME, using="h1") WebElement homeLoanKeyWords;
	
	public void clickOnCallBackLink(){
		callBackLink.click();
	}

	public void verifyHomeLoansKeyWords(){
		Assert.assertEquals(homeLoanKeyWords.getText(), verificationKeyWords, "Expected key words not presented");
		
	}
	
	public void waitCallBackLink() {
		wait20 = new WebDriverWait(driver, 20);
		wait20.until(ExpectedConditions.invisibilityOf(callBackLink));
	}
	
}
