package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

public class HomePage {
		WebDriver driver;
		final String verificationKeyWords = "OUR RANGE OF PERSONAL PRODUCTS";
		
		public HomePage(WebDriver driver){
			this.driver=driver;
			PageFactory.initElements(driver, this);
		}
		
		@FindBy(how=How.LINK_TEXT, using="Home loans") WebElement homeLoansLink;
		@FindBy(how=How.XPATH, using="//h2[@id='section-our-range-of-personal-products']") WebElement homePageKeyWords;
		
		public void clickOnHomeLoansLink(){
			homeLoansLink.click();
		}

		public void verifyHomePageKeyWords(){
			Assert.assertEquals(homePageKeyWords.getText(), verificationKeyWords, "Expected key words not presented");
			
		}
	}

