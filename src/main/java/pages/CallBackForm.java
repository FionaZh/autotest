package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

public class CallBackForm {
	
	WebDriver driver;
	String verificationKeyWords = "NAB CONTACT CENTRE CALL BACK FORM";
	String[] states = {"ACT","NSW","NT","QLD","SA","VIC","TAS","WA"};
	
	public CallBackForm(WebDriver driver){
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how=How.XPATH, using="//h1[@id='Page1']") WebElement callBackFormKeyWords;
	@FindBy(how=How.XPATH, using="//span[contains(text(),'No')]") WebElement notExistingCustomerOption;
	@FindBy(how=How.ID, using="field-page-Page1-aboutYou-firstName") WebElement firstNameTextBox;
	@FindBy(how=How.ID, using="field-page-Page1-aboutYou-lastName") WebElement lastNameTextBox;
	@FindBy(how=How.XPATH, using="//div[@class='css-1hwfws3 react-select__value-container']") WebElement stateInput;
	@FindBy(how=How.ID, using="field-page-Page1-aboutYou-phoneNumber") WebElement phoneNumberTextBox;
	@FindBy(how=How.ID, using="field-page-Page1-aboutYou-email") WebElement emailTextBox;
	@FindBy(how=How.ID, using="page-Page1-btnGroup-submitBtn") WebElement submitButton;
	
	public void verifyCallBackFormKeyWords(){
		Assert.assertEquals(callBackFormKeyWords.getText(), verificationKeyWords, "Expected key words not presented");
		
	}
	
	public void clickOnNotExistingCustomerOption(){
		notExistingCustomerOption.click();
	}

	public void setFristName(String firstName){
		firstNameTextBox.sendKeys(firstName);
	}
	public void setLastName(String lastName){
		lastNameTextBox.sendKeys(lastName);
	}
	public void clickOnStateInput(){
		stateInput.click();
	}
	public void chooseState(String state){
		String xpath = "//div[@id='react-select-3-option-0']";
		for (int i=0;i<8;i++) {
			if (state.equalsIgnoreCase(states[i])) {
				xpath = "//div[@id='react-select-3-option-" +i+"']";
				break;
			}
			
		}
			
		driver.findElement(By.xpath(xpath)).click();
	}
	
	public void setPhoneNumber(String phoneNumber){
		phoneNumberTextBox.sendKeys(phoneNumber);
	}
	
	public void setEmail(String email){
		emailTextBox.sendKeys(email);
	}
	
	public void clickOnSubmitButton() throws Exception{
		Actions actions = new Actions(driver);
		actions.moveToElement(submitButton).click().build().perform();
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", submitButton);
		//submitButton.click();
	}
}
