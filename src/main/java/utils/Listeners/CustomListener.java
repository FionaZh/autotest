package utils.Listeners;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class CustomListener implements ITestListener {
	String workingDir = System.getProperty("user.dir");
	private ExtentReports extentR=new ExtentReports(workingDir+"\\ExtentReports\\ExtentReportResults.html", true);
	private ExtentTest extentT;
	private WebDriver driver;
	
	
	@Override
	public void onTestStart(ITestResult result) {
		
	
		System.out.println("---------------------------------------------------");
		System.out.println("Method "+result.getName() + " Start");

	}

	@Override
	public void onTestSuccess(ITestResult result) {
		// TODO Auto-generated method stub
		System.out.println("Method " + result.getName() + " Success");
	
		extentT.log(LogStatus.PASS, "Test Success "+result.getName());
	
	}

	@Override
	public void onTestFailure(ITestResult result) {
		// TODO Auto-generated method stub
		
		System.out.println("Method " + result.getName() + " Failed");
		extentT.log(LogStatus.FAIL, "Test Failed: "+result.getName());
		
		driver=(WebDriver)result.getTestContext().getAttribute("driver");
		String base64Screenshot = "data:image/png;base64,"+((TakesScreenshot)driver).
                getScreenshotAs(OutputType.BASE64);
				
 
        //Extentreports log and screenshot operations for failed tests.
       extentT.log(LogStatus.FAIL,"Test Failed",
                extentT.addBase64ScreenShot(base64Screenshot));       
      
	}

	@Override
	public void onTestSkipped(ITestResult result) {
		
		System.out.println("Method " + result.getName() + " Skipped");
		extentT.log(LogStatus.SKIP, "Test Skipped "+result.getName());
	}

	@Override
	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
			
		System.out.println("Method " + result.getName() + " Failed but Within Success Percentage");
		extentT.log(LogStatus.SKIP, "Test is failed but within Success Percentage." +result.getName());
	}

	@Override
	public void onStart(ITestContext context) {
			
		System.out.println("Test "+context.getName()+" Start");
		
		extentT=extentR.startTest(context.getName(), "Start Test");
	}

	@Override
	public void onFinish(ITestContext context) {
		
		System.out.println("Test "+context.getName()+" End");
		
		extentR.endTest(extentT);
		extentR.flush();
		
	}

}
