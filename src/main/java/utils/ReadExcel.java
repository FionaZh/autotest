package utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadExcel {
	//Read all cell values to 2-dimensioned 
		//Excel row number and col number start from 0.
		public static String[][] readExcel(String filePath, String sheetName, int colNumber) throws Exception 
		{
			String[][] result = null;		
			File file=new File(filePath);
			int i, j, rowNumber;
			
			System.out.println("Start reading files");
			
			try 
			{
				InputStream is = new FileInputStream(file);
				XSSFWorkbook wb = new XSSFWorkbook(is);
				DataFormatter df = new DataFormatter();
				XSSFSheet sheetContent = wb.getSheet(sheetName);
				
						
				rowNumber = sheetContent.getLastRowNum();
				
				//System.out.println("Row number is: " + rowNumber);
				result = new String[rowNumber][colNumber];
				
				
				for(i=0; i<sheetContent.getLastRowNum(); i++) 
				{
					
						for(j=0; j<colNumber; j++)
						{
							//Read from 2nd line (1st line is reserved for header)
							result[i][j]=df.formatCellValue(sheetContent.getRow(i+1).getCell(j));
							System.out.println(result[i][j]);
					
						}
				}
				
				wb.close();
					
		
			}catch(FileNotFoundException e) 
			{
				System.out.println("Can't find file: " + filePath);
			}
							
			return result;
			
			}	  
}
