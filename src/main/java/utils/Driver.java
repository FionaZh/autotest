package utils;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Driver {
	public static WebDriver funcSetupDriver(WebDriver driver, String browserName,String headless)
	{
		switch (browserName.toLowerCase())
		{
		case "chrome":
			
			WebDriverManager.chromedriver().setup();
			ChromeOptions options = new ChromeOptions();
			options.addArguments("--no-sandbox");
			options.setExperimentalOption("useAutomationExtension", false);
			
			
			if(Boolean.valueOf(headless.trim())) {
				options.addArguments("window-size=1920,1080");
				options.addArguments("headless");
				//options.addArguments("--remote-debugging-port=9222");
				options.addArguments("--disable-gpu");
			}
			driver = new ChromeDriver(options);
			break;
		
		case "firefox":
			WebDriverManager.firefoxdriver().setup();
			driver = new FirefoxDriver();
			break;
		case "edge":
			WebDriverManager.edgedriver().setup();
	        driver = new EdgeDriver();
			break;
		case "safari":
			
			driver = new SafariDriver();
			break;
		}
		utils.Listeners.TestListener.driver=driver;
		driver.manage().window().maximize();		 
		driver.manage().timeouts().implicitlyWait(6, TimeUnit.SECONDS);	  
		
		
		return driver;
	}
	
	
}
