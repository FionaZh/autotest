package tests;

import java.util.Set;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import pages.*;
import utils.*;

public class HomeLoanEnquery {
	WebDriverWait wait10;
	WebDriver driver;
	HomePage homePage;
	HomeLoans homeLoans;
	CallBackHomeLoanTopic callBackHomeLoanTopic;
	CallBackForm callBackForm;
	Feedback feedback;
	
	private static String projectPath = System.getProperty("user.dir");
	private static String dataPath = "/src/main/resources/Data/PersonalInfo.xlsx";
	
	final static String topicPageTitle = "directory";
	
	// Launch driver
	@BeforeClass(alwaysRun = true)
	@Parameters(value= {"URL","headless"})
	public void LaunchBrowser(String URL, @Optional("False") String headless, ITestContext context)
	{
		driver = Driver.funcSetupDriver(driver, "chrome", headless);
		context.setAttribute("driver", driver);
		wait10 = new WebDriverWait(driver, 10);
		driver.navigate().to(URL);
	}
	
	// Create the Data Provider and give the data provider a name
	@DataProvider(name = "personal-infomation-data-provider")
	public String[][] personalInfoDataProvider() throws Exception {
		String filePath = projectPath + dataPath;
		String sheetName="sheet1";
		int colNumber=5;

		return ReadExcel.readExcel(filePath, sheetName, colNumber);
	}
		
	@Test(priority=1)
	public void verifyHomePage() throws Exception {

		//Operations on home page
		homePage = new HomePage(driver);
		homePage.verifyHomePageKeyWords();
		homePage.clickOnHomeLoansLink();

	}
	@Test(priority=2)
	public void verifyHomeLoanPage() throws Exception {
	
		//Home loan page
		homeLoans = new HomeLoans(driver);
		homeLoans.verifyHomeLoansKeyWords();
		homeLoans.clickOnCallBackLink();
	
		
	}
	@Test(priority=3)
	public void verifyHomeLoanTopicPage() throws Exception {
	
		//Home loan topic page
		callBackHomeLoanTopic = new CallBackHomeLoanTopic(driver);

		Assert.assertTrue(driver.getTitle().contains(topicPageTitle), "Key words not presented in topic page title");
		//callBackHomeLoanTopic.verifyHomeLoanTopicKeyWords();
		callBackHomeLoanTopic.clickOnNewHomeLoansRadio();
		callBackHomeLoanTopic.clickOnNextButton();
	
		String parentWindow = driver.getWindowHandle();
		Set <String> windows = driver.getWindowHandles();
		
		for (String window:windows) {
			if (!window.equals(parentWindow)) {
				driver.switchTo().window(window); 
			} 
		}
			
	}
	
	@Test(priority=4,dataProvider="personal-infomation-data-provider")
	public void verifyCallBackFormPage(String firstName,String lastName,String state,String phoneNumber,String email) throws Exception {

		//Fill call back form and submit
		callBackForm = new CallBackForm(driver);
		callBackForm.verifyCallBackFormKeyWords();
		callBackForm.clickOnNotExistingCustomerOption();
		callBackForm.setFristName(firstName);
		callBackForm.setLastName(lastName);
		callBackForm.clickOnStateInput();
		callBackForm.chooseState(state);
		callBackForm.setPhoneNumber(phoneNumber);
		callBackForm.setEmail(email);
		callBackForm.clickOnSubmitButton();

	}
	
	@Test(priority=5)
	public void verifyFeedbackPage() throws Exception {
	
		//Feedback page after submit call back form
		feedback = new Feedback(driver);
		feedback.verifyFeedbackKeyWords();
			
	}
	
	@AfterClass(alwaysRun = true)
	public void TeardownTest()
	{
		driver .quit();
	}
	
}
